package com.example.chatterbox;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.chatterbox.SQLite.Internal_DB;

public class MainActivity extends AppCompatActivity {
    Internal_DB internal_db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        internal_db = new Internal_DB(this);
        int check_User = internal_db.check_Registration(1);

        if (check_User == 0){
            //user is not registered so take the user to the registration panel
            Intent registerUser = new Intent(MainActivity.this, Register_Self.class);
            startActivity(registerUser);
        } else {
            //check if the user has a buddy
            check_User = internal_db.check_Registration(2);

            if(check_User == 0){
                //user has no chat buddy so lets take them to register one
                Intent registerUser = new Intent(MainActivity.this, Register_Buddy.class);
                startActivity(registerUser);
            } else {
                setContentView(R.layout.activity_main);
            }//end if.. else
        }//end if... else

    }//end of oncreate

    //check if the user is registered
}//end of class
