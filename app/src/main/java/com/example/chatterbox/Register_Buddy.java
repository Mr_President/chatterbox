package com.example.chatterbox;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.chatterbox.SQLite.Internal_DB;

public class Register_Buddy extends AppCompatActivity {
    Internal_DB internal_db = new Internal_DB(this);
    EditText etUserName, etContacts;
    Button btnRegisterBuddy;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_buddy);

        etUserName = findViewById (R.id.etUserName);
        etContacts = findViewById (R.id.etContacts);
        btnRegisterBuddy = findViewById (R.id.btnRegisterBuddy);

        btnRegisterBuddy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean registerBuddy = internal_db.registration(String.valueOf(etUserName.getText()), String.valueOf(etContacts.getText()), 2);

                //execute the registration
                if( registerBuddy = true){
                    Toast.makeText(Register_Buddy.this, "Chat Buddy\nRegistered Successfully", Toast.LENGTH_LONG).show();
                    Intent mainAct = new Intent(Register_Buddy.this, MainActivity.class);
                    startActivity(mainAct);
                } else {
                    Toast.makeText(Register_Buddy.this, "Registration Error", Toast.LENGTH_LONG).show();
                }//end of if.. else
            }
        });
    }
}

