package com.example.chatterbox;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.chatterbox.SQLite.Internal_DB;

public class Register_Self extends AppCompatActivity {
    EditText etUserName, etContacts;
    Button btnRegisterSelf, btnRegBuddy;
    Internal_DB internal_db = new Internal_DB(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_self);

        etUserName = findViewById(R.id.etUserName);
        etContacts = findViewById(R.id.etContacts);
        btnRegisterSelf = findViewById(R.id.btnRegisterSelf);
        btnRegBuddy = findViewById(R.id.btnRegBuddy);

        btnRegisterSelf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean register = internal_db.registration(String.valueOf(etUserName.getText()), String.valueOf(etContacts.getText()), 1);

                if( register = true){
                    Toast.makeText(Register_Self.this, "Registered Successfully", Toast.LENGTH_LONG).show();
                    Intent mainAct = new Intent(Register_Self.this, MainActivity.class);
                    startActivity(mainAct);
                } else {
                    Toast.makeText(Register_Self.this, "Registration Error", Toast.LENGTH_LONG).show();
                }//end of if.. else
            }
        });//end of registration button

        btnRegBuddy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regBuddy = new Intent(Register_Self.this, Register_Buddy.class);
                startActivity(regBuddy);
            }
        });
    }
}