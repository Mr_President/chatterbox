package com.example.chatterbox.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class Internal_DB extends SQLiteOpenHelper{
    private static final String DB_NAME = "Chatterbox";//set the db name
    private static final int VERSION = 1;
    private static final String TABLE = "User_Details";//this table will hold the details of the user and their chat buddy
    private static final String COL_1 = "ID";//for the primary key and also identification of who the user and chat buddy is.
    //the user will have the ID 1 and the chat buddy will have the ID 2
    private static final String COL_2 = "User_Name";//standard username for the chat
    private static final String COL_3 = "Contact";// the contacts for user and chat buddy.

    //our default constructor
    public Internal_DB(Context context){
        super(context, DB_NAME, null, VERSION);
    }//end of db method

    @Override
    public void onCreate(SQLiteDatabase db) {
        //when called it will create our table
        db.execSQL("create table "+TABLE+"(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                "User_Name TEXT NOT NULL," +
                "Contact TEXT NOT NULL);");
    }//end of onCreate

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //it will delete and recreate our table in the event that there are any upgrades
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE);
        onCreate(db);
    }//end of onUpgrade

    //lets do a method for registering either our User or our user's buddy
    public boolean registration (String username, String contacts, int userID){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        //pass the parameters we need for the query
        contentValues.put(COL_1, userID);
        contentValues.put(COL_2, username);
        contentValues.put(COL_3, contacts);

        //execute the query
        long result = db.insert(TABLE, null, contentValues);

        //ensure the insert was successful
        if(result == -1){
            //the isert failed
            return false;
        } else {
            return true;
        }
    }//end of registering a user/chat buddy

    //lets check if our user is registered to use the app and whether they have a chat buddy
    public int check_Registration(int checkReg){
        SQLiteDatabase db = this.getWritableDatabase();
            int result = 0;
            Cursor res = db.rawQuery("select * from "+TABLE+" where "+COL_1+" = "+checkReg, null);

            //check if we have found the user/chat buddy
        if (res.getCount()==0) {
            result = 0; //meanimg that the user/chat buddy is registered
        } else {
            result = 1; //meaning that the user/chat buddy is not registered
        }

        return result;
        }//end of checking for user registration

    }//end of database class
